package ds

import (
	"fmt"
	"math/rand"
	"strings"
)

type ArrayList[V Number] []V

func NewArrayList[V Number](l int) ArrayList[V] {
	return make(ArrayList[V], l)
}

func NewRandomArrayList[V Number](l, max int) ArrayList[V] {
	a := make(ArrayList[V], l)
	for i := 0; i < l; i++ {
		if max > 0 {
			a[i] = V(rand.Intn(max))
			continue
		}
		a[i] = V(rand.Int())
	}
	return a
}

func NewAscendingArrayList[V Number](l int) ArrayList[V] {
	a := make(ArrayList[V], l)
	for i := 0; i < l; i++ {
		a[i] = V(i)
	}
	return a
}

func NewDescendingArrayList[V Number](l int) ArrayList[V] {
	a := make(ArrayList[V], l)
	j := l - 1
	for i := 0; i < l; i++ {
		a[i] = V(j)
		j--
	}
	return a
}

func (a ArrayList[V]) FirstN(n int) string {
	var sb strings.Builder
	j := n

	l := len(a)
	if j > l {
		j = l
	}

	for i := 0; i < j; i++ {
		fmt.Fprintf(&sb, "%v ", a[i])
	}

	if j < l {
		fmt.Fprintf(&sb, "... ")
	}

	return sb.String()
}

func (a ArrayList[V]) Sorted() bool {
	for i := len(a) - 1; i > 0; i-- {
		if a[i] < a[i-1] {
			return false
		}
	}
	return true
}

func (a ArrayList[V]) Swap(i, j int) {
	aux := a[i]
	a[i] = a[j]
	a[j] = aux
}
