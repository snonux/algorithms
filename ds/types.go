package ds

import (
	"golang.org/x/exp/constraints"
)

type Integer interface {
	constraints.Integer
}

type Number interface {
	constraints.Integer | constraints.Float
}
