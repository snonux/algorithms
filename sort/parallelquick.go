package sort

import (
	"sync"

	"codeberg.org/snonux/algorithms/ds"
)

func ParallelQuick[V ds.Number](a ds.ArrayList[V]) ds.ArrayList[V] {
	//Shuffle(a)
	parallelQuick(a)
	return a
}

func parallelQuick[V ds.Number](a ds.ArrayList[V]) {
	l := len(a)

	if l < 1000 {
		quick(a)
		return
	}

	j := quickPartition(a)
	var wg sync.WaitGroup
	wg.Add(2)

	go func() {
		parallelQuick(a[0:j])
		wg.Done()
	}()
	go func() {
		parallelQuick(a[j+1:])
		wg.Done()
	}()

	wg.Wait()
}
