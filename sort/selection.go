package sort

import (
	"codeberg.org/snonux/algorithms/ds"
)

func Selection[V ds.Number](a ds.ArrayList[V]) ds.ArrayList[V] {
	l := len(a)

	for i := 0; i < l; i++ {
		min := i
		for j := i + 1; j < l; j++ {
			if a[min] > a[j] {
				min = j
			}
		}
		if min == i {
			continue
		}
		a.Swap(i, min)
	}

	return a
}
