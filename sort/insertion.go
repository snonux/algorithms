package sort

import (
	"codeberg.org/snonux/algorithms/ds"
)

func Insertion[V ds.Number](a ds.ArrayList[V]) ds.ArrayList[V] {
	for i, _ := range a {
		for j := i; j > 0; j-- {
			if a[j] > a[j-1] {
				break
			}
			a.Swap(j, j-1)
		}
	}

	return a
}
