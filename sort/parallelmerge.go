package sort

import (
	"sync"

	"codeberg.org/snonux/algorithms/ds"
)

func ParallelMerge[V ds.Number](a ds.ArrayList[V]) ds.ArrayList[V] {
	parallelMerge[V](a, make(ds.ArrayList[V], len(a)))
	return a
}

func parallelMerge[V ds.Number](a, aux ds.ArrayList[V]) {
	l := len(a)

	if l < 1000 {
		mergeSort(a, aux)
		return
	}

	mi := l / 2
	var wg sync.WaitGroup
	wg.Add(2)

	go func() {
		parallelMerge(a[0:mi], aux[0:mi])
		wg.Done()
	}()

	go func() {
		parallelMerge(a[mi:], aux[mi:])
		wg.Done()
	}()

	wg.Wait()
	merge(a, aux, 0, mi, l-1)
}
