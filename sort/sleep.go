package sort

import (
	"codeberg.org/snonux/algorithms/ds"
	"sync"
	"time"
)

func Sleep[V ds.Integer](a ds.ArrayList[V]) ds.ArrayList[V] {
	sorted := ds.NewArrayList[V](len(a))

	numCh := make(chan V)
	var wg sync.WaitGroup
	wg.Add(len(a))

	go func() {
		wg.Wait()
		close(numCh)
	}()

	for _, num := range a {
		go func(num V) {
			defer wg.Done()
			time.Sleep(time.Duration(num) * time.Second)
			numCh <- num
		}(num)
	}

	for num := range numCh {
		sorted = append(sorted, num)
	}

	return sorted
}
