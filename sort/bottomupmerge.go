package sort

import (
	"codeberg.org/snonux/algorithms/ds"
)

func BottomUpMerge[V ds.Number](a ds.ArrayList[V]) ds.ArrayList[V] {
	l := len(a)
	aux := make(ds.ArrayList[V], l)

	for sz := 1; sz < l; sz = sz + sz {
		for lo := 0; lo < l-sz; lo += sz + sz {
			merge(a, aux, lo, lo+sz, min(lo+sz+sz-1, l-1))
		}
	}

	return a
}

func min(a, b int) int {
	if a <= b {
		return a
	}
	return b
}
