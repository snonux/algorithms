package sort

import (
	"math/rand"

	"codeberg.org/snonux/algorithms/ds"
)

func Shuffle[V ds.Number](a ds.ArrayList[V]) ds.ArrayList[V] {
	l := len(a)

	for i := 0; i < l; i++ {
		r := l - rand.Intn(l-i) - 1
		a.Swap(i, r)
	}

	return a
}
