package queue

import (
	"codeberg.org/snonux/algorithms/ds"
)

type ElementaryPriority[T ds.Number] struct {
	a ds.ArrayList[T]
	// Initial capacity
	capacity int
}

func NewElementaryPriority[T ds.Number](capacity int) *ElementaryPriority[T] {
	return &ElementaryPriority[T]{make(ds.ArrayList[T], 0, capacity), capacity}
}

func (q *ElementaryPriority[T]) Insert(a T) {
	q.a = append(q.a, a)
}

func (q *ElementaryPriority[T]) Max() (max T) {
	_, max = q.max()
	return
}

func (q *ElementaryPriority[T]) DeleteMax() T {
	if q.Empty() {
		return 0
	}

	ind, max := q.max()
	for i := ind + 1; i < q.Size(); i++ {
		q.a[i-1] = q.a[i]
	}
	q.a = q.a[0 : len(q.a)-1]

	return max
}

func (q *ElementaryPriority[T]) Empty() bool {
	return q.Size() == 0
}

func (q *ElementaryPriority[T]) Size() int {
	return len(q.a)
}

func (q *ElementaryPriority[T]) Clear() {
	q.a = make(ds.ArrayList[T], 0, q.capacity)
}

func (q *ElementaryPriority[T]) max() (ind int, max T) {
	for i, a := range q.a {
		if a > max {
			ind, max = i, a
		}
	}
	return ind, max
}
