package search

import (
	"codeberg.org/snonux/algorithms/ds"
)

type GoMap[K,V ds.Number] map[K]V

func NewGoMap[K,V ds.Number] () GoMap[K,V] {
	return make(GoMap[K,V])
}

func (m GoMap[K,V]) Empty() bool {
	return m.Size() == 0
}

func (m GoMap[K,V]) Size() int {
	return len(m)
}

func (m GoMap[K,V]) Put(key K, val V) {
	m[key] = val
}

func (m GoMap[K,V]) Get(key K) (V, error) {
	val, ok := m[key]
	if !ok {
		return 0, NotFound
	}
	return val, nil
}

func (m GoMap[K,V]) Del(key K) (V, error) {
	val, ok := m[key]
	if !ok {
		return 0, NotFound
	}
	delete(m, key)
	return val, nil
}
