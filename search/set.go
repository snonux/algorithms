package search

import (
	"fmt"
	"codeberg.org/snonux/algorithms/ds"
)

var (
	NotFound       = fmt.Errorf("could not find entry")
	NotImplemented = fmt.Errorf("method not implemented")
)

type Set[K ds.Integer,V ds.Number] interface {
	Empty() bool
	Size() int
	Put(key K, val V)
	Get(key K) (V, error)
	Del(key K) (V, error)
}
