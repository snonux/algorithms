package search

import (
	"fmt"
	"codeberg.org/snonux/algorithms/ds"
)

type node[K, V ds.Number] struct {
	key   K
	val   V
	left  *node[K,V]
	right *node[K,V]
}

func (n *node[K,V]) String() string {
	recurse := func(n *node[K,V]) string {
		if n == nil {
			return ""
		}
		return n.String()
	}

	return fmt.Sprintf("node[K,V]{%v:%v,%s,%s}",
		n.key,
		n.val,
		recurse(n.left),
		recurse(n.right))
}

type BST[K,V ds.Number] struct {
	root *node[K,V]
	size int
}

func NewBST[K,V ds.Number]() *BST[K,V] {
	return &BST[K,V]{}
}

func (t *BST[K,V]) String() string {
	if t.Empty() {
		return "BST[K,V]{}"
	}

	return fmt.Sprintf("BST[K,V]{%s}", t.root)
}

func (t *BST[K,V]) Empty() bool {
	return t.root == nil
}

func (t *BST[K,V]) Size() int {
	return t.size
}

func (t *BST[K,V]) Put(key K, val V) {
	if t.Empty() {
		t.root = &node[K,V]{key, val, nil, nil}
		t.size++
		return
	}
	ptr, _, err := t.search(&t.root, key)
	switch err {
	case nil:
		// key already in the tree
		return
	case NotFound:
		*ptr = &node[K,V]{key, val, nil, nil}
		t.size++
		return
	default:
		panic(err)
	}
}

func (t *BST[K,V]) Get(key K) (V, error) {
	_, n, err := t.search(&t.root, key)
	if err != nil {
		return 0, err
	}
	return n.val, nil
}

func (t *BST[K,V]) Del(key K) (V, error) {
	ptr, n, err := t.search(&t.root, key)
	if err != nil {
		return 0, err
	}
	t.size--

	// Case 1: n is leaf node
	// Case 2: n has one child
	// Case 3: n has two childs

	switch {
	case n.left == nil:
		if n.right == nil {
			// I am a leaf node
			*ptr = nil
			return n.val, nil
		}
		// I have a right child
		*ptr = n.right
		return n.val, nil

	case n.right == nil:
		// I have a left child
		*ptr = n.left
		return n.val, nil
	default:
		// I have two children!

		o, err := t.deleteMin(&n.right)
		if err != nil {
			return 0, err
		}

		o.left = n.left
		o.right = n.right
		*ptr = o

		return n.val, nil
	}
}

func (t *BST[K,V]) search(ptr **node[K,V], key K) (**node[K,V], *node[K,V], error) {
	n := *ptr
	if n == nil {
		return ptr, nil, NotFound
	}

	switch {
	case key < n.key:
		return t.search(&n.left, key)
	case n.key < key:
		return t.search(&n.right, key)
	default:
		return ptr, n, nil
	}
}

func (t *BST[K,V]) deleteMin(ptr **node[K,V]) (*node[K,V], error) {
	ptr, n, err := t.min(ptr)
	if err != nil {
		return nil, err
	}

	*ptr = n.right
	n.right = nil
	return n, nil
}

func (t *BST[K,V]) min(ptr **node[K,V]) (**node[K,V], *node[K,V], error) {
	n := *ptr
	if n == nil {
		return nil, nil, NotFound
	}

	for {
		if n.left == nil {
			return ptr, n, nil
		}
		ptr = &n.left
		n = *ptr
	}
}
