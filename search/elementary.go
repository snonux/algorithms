package search

import (
	"codeberg.org/snonux/algorithms/ds"
)

type ElementaryElem[K, V ds.Number] struct {
	key  K
	val  V
	next *ElementaryElem[K,V]
}

type Elementary[K,V ds.Number] struct {
	root *ElementaryElem[K,V]
	size int
}

func NewElementary[K, V ds.Number]() *Elementary[K,V] {
	return &Elementary[K,V]{}
}

func (s *Elementary[K,V]) Empty() bool {
	return s.root == nil
}

func (s *Elementary[K,V]) Size() int {
	return s.size
}

func (s *Elementary[K,V]) Put(key K, val V) {
	if s.Empty() {
		s.root = &ElementaryElem[K,V]{key, val, nil}
		s.size++
		return
	}

	elem := s.root

	for {
		if elem.key == key {
			elem.val = val
			return
		}
		if elem.next == nil {
			elem.next = &ElementaryElem[K,V]{key, val, nil}
			s.size++
			return
		}
		elem = elem.next
	}
}

func (s *Elementary[K,V]) Get(key K) (V, error) {
	elem := s.root

	for elem != nil {
		if elem.key == key {
			return elem.val, nil
		}
		elem = elem.next
	}

	return 0, NotFound
}

func (s *Elementary[K,V]) Del(key K) (V, error) {
	if s.Empty() {
		return 0, NotFound
	}

	if s.root.key == key {
		defer func() {
			s.root = s.root.next
		}()
		s.size--
		return s.root.val, nil
	}

	elem := s.root
	for elem.next != nil {
		if elem.next.key == key {
			defer func() {
				elem.next = elem.next.next
			}()
			s.size--
			return elem.next.val, nil
		}
		elem = elem.next
	}

	return 0, NotFound
}
